namespace Microlight.MicroUI {
    public enum MicroCursorStates {
        Normal,
        Hovering,
        Interactable,
        NonInteractable,
        Dragging,
        Clicked,
        Waiting,
        Friendly,
        Enemy,
    }
}