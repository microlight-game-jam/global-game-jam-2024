namespace Microlight.MicroUI {
    // ****************************************************************************************************
    // Interface for scripts that are source of dynamic content for MicroTooltips
    // ****************************************************************************************************
    public interface IMicroTooltipContent {
        public string GetMicroTooltipContent();
    }
}