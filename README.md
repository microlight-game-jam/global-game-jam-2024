# GGJ24

Whack-a-mole game.

## About

For GGJ 2024 in Zagreb, we created a game where the player takes the role of a boss in a corporation. Workers tend to slack off because of the boring corporate job but the boss likes money.
Actually, money is the only thing that makes the boss laugh these days and you want to make yourself laugh. Whack employees who are slacking off to increase productivity but don't slap innocent workers because legal fees are expensive, which we don't like.

![Main Menu](gitData/main_menu.png?raw=true "Main Menu")

## Technology

Game is made in Unity game engine with the help of tools like Blender, Photopea and free online resources.